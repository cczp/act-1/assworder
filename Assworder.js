var Assword = function(length, lengthType) {
	if (length < 1) {
		throw new RangeError("assword length is too small");
	} else if (length > 4294967295) {
		throw new RangeError("assword length is too big");
	} else if (!isFinite(length)) {
		throw new TypeError("assword length is not a number");
	}
	
	var buffer = [];
	
	for (var i = 0; i < length; i++) {
		buffer.push(Assword.getRandomChar());
	}
	
	var assword = buffer.join("");
	
	switch (lengthType) {
		case "String":
			return new String(Assword.stringTruncate(assword, length));
		case "Byte":
			return new String(Assword.byteTruncate(assword, length));
		default:
			return new String(assword);
	}
};
Assword.stringTruncate = function(assword, length) {
	return assword.slice(0, length);
};

if (typeof window != "undefined") {
	Assword.getRandomChar = function() {
		return String.fromCodePoint(new DataView(
			crypto.getRandomValues(new Uint8Array(4)).buffer
		).getUint32(0) % 1114111);
	};
	
	Assword.byteTruncate = function(assword, length) {
		return (new TextDecoder("utf-8")).decode(
			(new TextEncoder()).encode(assword).slice(0, length)
		);
	};
	
	window.Assword = Assword;
} else if (typeof module != "undefined") {
	var crypto = require("crypto");
	
	Assword.getRandomChar = function() {
		return String.fromCodePoint(
			crypto.randomBytes(4).readUInt32BE(0) % 1114111
		);
	};
	
	Assword.byteTruncate = function(assword, length) {
		return Buffer.from(assword).slice(0, length).toString();
	};
	
	module.exports = Assword;
} else {
	throw new TypeError("your environment isn't supported");
}
